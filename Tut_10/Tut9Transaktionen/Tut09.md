# Tut 9 - Transaktionsmanagement
&rarr; [Quizizz](https://quizizz.com/admin/quiz/60cba47abfa6d2001b689f37)

## Begriffe
- R(A): Lesen von A
- W(A): Schreiben nach A
- BOT (Begin of Transaction)
- EOT (End of Transaction)/Commit
- ABORT – Transaktion abbrechen
- l(A) - Sperren
- u(A) - Entsperren
- xl(A) - Exclusive lock/Schreibsperre
- sl(A) - Shared lock/Lesesperre