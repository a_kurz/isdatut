# Tut5 - FDs und Normalisierung

## Warm-Up
- [Quizziz](https://quizizz.com/admin/quiz/60a620c314a089001b98bef6)
- "[Update 19.05](https://isis.tu-berlin.de/mod/forum/discuss.php?d=384375): Das Prüfungsamt nimmt gerade keine Art von Anmeldungen an. Bitte gedulden Sie sich bis die TU mehr Informationen über die Anmeldefristverlängerung ankündigt."

## Functional Dependency (FD)
Eine Relation wird durch Attribute definiert. Bestimmen einige dieser Attribute eindeutig die Werte anderer Attribute, so spricht man von **funktionaler Abhängigkeit**. 

## Schlüssel
ER-Diagramm &rarr; Relation
Falls die Relation von einem Relationship-Typ stammt 
- Falls die Beziehung **m:n** ist, besteht der Schlüssel aus den Schlüsselattributen der verbundenen Entity-Typen. 
- Falls die Beziehung **1:n** ist, besteht der Schlüssel aus den Schlüsselattributen des Entity-Typs der n-Seite. 
- Falls die Beziehung **1:1** ist, besteht der Schlüssel aus den Schlüsselattributen eines der beiden beteiligten Entity-Typen (egal welcher).

## Hülle
Die Hülle einer Menge von Attribute ist die Menge an Attributen, die zusätzlich alle indirekt über Functional Dependencies (FDs) erreichbaren Paare enthält. (&rarr; Rekursive Wiederholung bis keine neuen Elemente mehr zur Menge hinzukommen)

## Armstrong Axiome
Mit Hilfe der Axiome von Armstrong (auch Armstrong-Axiome) lassen sich aus einer Menge von funktionalen Abhängigkeiten, die auf einer Relation gelten, weitere funktionale Abhängigkeiten ableiten. Die *folgenden drei Regeln reichen aus, um alle funktionalen Abhängigkeiten herzuleiten (completeness)*:

### Reflexivität
Eine Menge von Attributen bestimmt eindeutig die Werte einer Teilmenge dieser Attribute (triviale Abhängigkeit), das heißt, **β⊆α⇒α→β**

### Akkumulation
**Gilt α→β, so gilt auch αγ→βγ** für jede Menge von Attributen γ der Relation. (auch: Augmentation)

### Transitivität
Gilt **α→β und β→γ**, so gilt auch **α→γ**

Um Herleitungen einfacher zu gestalten, können zusätzlich die folgenden *(abgeleiteten) Regeln* verwendet werden:

- Dekomposition: **α→βγ => α→β** UND α→γ
- Vereinigung: **α→β UND α→γ => α→βγ**
- Pseuditransitivität: **α→β UND βγ→δ => αγ→δ**

## Normalformen (NF)
Bauen aufeinander auf, die vorherige NF muss jeweils gegeben sein.

### 1NF
**Nur atomare Attribute**, keine Einschränkung bezüglich FD.

*Umformungsschritte*:
- Mengenwertige Attribute als eigene Relation 
- Strukturierte Attribute: Eine Spalte pro Attribut

### 2NF
**Nicht-Schlüssel-Attribut darf nicht vom Teilschlüssel abhängig sein.**

*Umformungsschritte*: Dekomposition
S(As,Cs,B,D) mit FDs AC → D, und A → B; Dekomposition in: S1(As,Cs,D) und S2(As,B) 

### 3NF
Jedes Nicht-Schlüssel-Attribut darf von keinem Schlüsselkandidaten transitiv abhängig sein. 

Einfach gesagt: Ein **Nichtschlüsselattribut darf nicht von einer Menge aus Nichtschlüsselattributen abhängig sein**. Ein Nichtschlüsselattribut darf also nur direkt von einem Primärschlüssel (bzw. einem Schlüsselkandidaten) abhängig sein. 

*Umformungsschritte*: 
R(As,Bs,C,D) mit C → D; Dekomposition zu R(As,Bs,C) und S(Cs,D)

### BCNF
Keine funktionale Abhängigkeit des Teilschlüssels vom Nichtschlüsselattribut.
+(Jede Relation mit nur zwei Attributen ist in BCNF)

*Umformungsschritte*: 
R(As,Bs,C,D) mit AB → C, AB → D und C  → B;  Dekomposition in R(As,Cs, D) und S(Cs,B)

## Fragen
- 

--- 
Quellen:

- [Normalisierung](https://de.wikipedia.org/wiki/Normalisierung_(Datenbank))
- [Armstrong-Axiome](https://de.wikipedia.org/wiki/Funktionale_Abh%C3%A4ngigkeit#Axiome_von_Armstrong)




