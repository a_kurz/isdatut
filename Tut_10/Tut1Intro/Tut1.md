# Tut1 - Einführung, Python, Jupyter

## Organisatorisches
- [Git-Repository](https://git.tu-berlin.de/aaronkurz/ISDA-tut-aaron) für das Tutorium mit Lösungen/Unterlagen/Links/...
- falls ihr Fragen habt, könnt ihr euch natürlich gerne jederzeit [per E-Mail bei mir melden](mailto:aaron.f.kurz@campus.tu-berlin.de) (schaut aber bitte zuerst in den Folien und im Forum nach, ob eure Frage schon beantwortet wurde)
- ihr könnt gerne auch [anonymes Feedback/Wünsche/Ideen](https://forms.gle/kb3q4z9HzvUmPYbHA) geben
- Tutoriums-Plan:

![Tut-Plan](Tut_Plan.png)

## Quiz
- [Kahoot-Quiz](https://create.kahoot.it/share/isda-tut1/a7d7d23b-b824-4772-a32c-5d7db5e345a5)
    - Anmerkung zu den Kahoot-Quizzes
        - Die Quizzes dienen nur zum leichteren Einstieg ins Tutorium und dem Auffrischen der Inhalte. Die Quizzes decken **bei weitem** nicht den ganzen Stoff ab. Falls ihr Fehler in den Kahoots findet oder Fragen dazu habt, meldet euch bei mir, ich werde das dann verbessern.

## Jupyter-Notebook
- [01_basics](https://git.tu-berlin.de/dima/isda/isda-ss21)

