# Tut4 - Relationaler Entwurf
- Nächste Frist/Termin
	- **31.05.2021: Ende QISPOS Anmeldungsfrist** (22 Uhr!)
- Hinweis: [Matrix-Chat beitreten](https://chat.tu-berlin.de/#/room/#isda-studis-2021:matrix.tu-berlin.de)
	- Funktioniert immer noch! 	

&rarr; [Quizziz](https://quizizz.com/admin/quiz/609d28f1b26d02001b43f822)

&rarr; Aufgabenblatt 04\_relationaler-entwurf.ipynb

## Grundalgorithmus
1. **Entity-Typ &rarr; Relation** mit gleichen Attributen
2. **Relationship-Typ &rarr; Relation**, Attribute = zugehörigen Attribute und Schlüsselattribute beteiligter Entity-Typen
3. **Verfeinere** den Entwurf
	1. Relationen zusammenlegen
	2. Normalisierung

Ausnahmen: [Schwache Entity-Typen](#schwache-entity-typen); [Generalisierung/Spezialisierung](#generalisierung)

### Zusammenlegen von Relationen
Man kann folgende Relationen kombinieren:

-  Die Relation für einen Entity-Typen E
-  Mit der Relation eines **1:n** Relationship-Typs R, falls E auf den **n-Seite** liegt

&rarr; bei **1:1** Relationship-Typen kann man selbst Entscheiden auf welche Seite diese Zusammenlegung stattfindet

## Schwache Entity-Typen
Besonderheiten:

- Relation eines schwachen Entity-Typs S muss **auch die Schlüsselattribute aller Entity-Typen, die über unterstützende Relationship-Typen erreicht werden**, enthalten
- Alle Relationen für (normale) Relationship-Typen, die S in Beziehung mit anderen Entity- Typen setzen, müssen ebenfalls alle diese Attribute enthalten
- Ein **unterstützender Relationship-Typ** muss hingegen gar **nicht** durch eine Relation **abgebildet** werden (wegen 1:n) (Ausnahme: unterstützender Relationship-Typ hat Attribute)

## Generalisierung
Drei Stile: [ER-Stil](#er-stil), [Objektorientierter Stil](#oo-stil), [Null-Stil](#null-stil)

### ER-Stil
- Für jeden Entity-Typ E der Hierarchie erzeuge eine Relation mit den **Schlüsselattributen des Wurzel-Entity-Typs und den Attributen von E**.
-  Die IST-Relationship erhält keine Relation.
-  Geerbte Schlüsselattribute werden für weitere Beziehungen verwendet.

## OO-Stil
![](oo-stil.png)

-  Erzeuge **Relation für jeden Teilbaum**.
-  Diese Relation repräsentiert die Entities, die genau diese Komponenten der Hierarchie besitzen.
	- Objekte gehören zu genau einer Klasse.

## Null-Stil
- Eine einzige Relation mit allen Attributen
- Nicht zutreffende Werte werden mit NULL aufgefüllt
- Man benötigt diskriminierendes Attribut, das die Subklasse bestimmt, zu der jedes Tupel, gehört (siehe [binäre Attrbute Zusatzinfos](#zusatzinfos))

## Zusatzinfos
- Null-Stil: bei overlapping werden binäre Typattribute gebraucht
- Zusammenlegen von Relationen: Fremdschlüssel können NULL sein
- Aggregation: Wie 1:n; also als wäre dazwischen ein 1:n/1:1 relationship-typ "besteht-aus"; man kann direkt zusammenlegen (siehe foliensatz "datenbankentwurf - beispiel" für beispielhafte Umsetzung)

## Fragen
- Können pezialisierte Entity-Typen eigene Schlüsselattribute haben?
    - Sollten sie nicht haben. 
- Können nicht-schlüsselattribute Null sein? (nicht Null-Stil) (ergo: kann nur der Fremdschlüssel Null sein ausser bei Null-Stil?)
    - Ja
- Pfeil-Notation bei Fremdschlüssel verpflichtend/optinal/empfehlenswert?
    - Empfehlenswert 


