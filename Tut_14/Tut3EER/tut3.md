# Tut3: EER-Modellierung

- [Quizziz](https://quizizz.com/admin/quiz/6093a91709406c001b7f496b)
- Nächste Frist/Termin
	- **31.05.2021: Ende QISPOS Anmeldungsfrist** (22 Uhr!)
- Hinweis: [Matrix-Chat beitreten](https://chat.tu-berlin.de/#/room/#isda-studis-2021:matrix.tu-berlin.de)
	- Funktioniert immer noch! 	
- &rarr; Aufgabenblatt 03\_modellierung\_eer
