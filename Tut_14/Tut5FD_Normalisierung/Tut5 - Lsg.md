# Tut5 - Lsg
## Aufgabe: FDs
1)
- typ->material
- Anzahl -> Typ, Farbe, Material
- typ -> Anzahl
- typ -> material, anzahl,far
- Farbe, Material, Anzahl -> Typ
- typ,farbe->material
- 
- Typ,... -> Allen anderen
2)
typ
3)
ne, es könnten neue Werte hinzukommen. FDs sollten immer zuvor definiert werden, sie beziehen sich auf das Schema, nicht auf die Instanz

## Aufgabe: Amstrong-Axiome
- A->B,C,D
- A->C
- F->E,F
- F->F (ginge mit alle; Reflexivität)
- (A,G) -> G
- B->D,C
- (A,F)->A,B,C,D,E,F

B -> BD -> C
Transitivität:
B -> C

### Schlüssel
1+2)
- G: SK? true
- (A,F): SK? true
- (A,G): SK? false
3)
- G: best; least attributes
- (A,F)

## Aufgabe FDs2
*Relation S1:*
|W|X|Y|Z|
|-|-|-|-|
|1|2|3|4|
|1|*3*|3|3|
|*2*|2|3|5|

*Relation S2:*
|W|X|Y|Z|
|-|-|-|-|
|1|2|3|4|
|2|2|*4*|5|
|1|6|*5*|2|


## Aufgabe Normalisierung Lieferant
Schlüssel hier *kursiv*!

**1NF**:true; 2NF?: false (BteilNr -> BteilBez)
Lieferant(*LieferantNr*, Ort, Entf)
Lieferung(*LieferantNr*,*BteilNr*, BteilBez, Anzahl, Bearbeiter)

**2NF**: true; 3NF?: Nein, wegen Ort -> Entf
Lieferung(*LieferantenNr*, *BteilNr*, Anzahl, Bearbeiter)
Bauteil(*BteilNr*, BauteilBez)
Lieferant(*LieferantNr*, Ort, Entf)

**3NF**: true; BCNF?: false, wegen Bearbeiter -> BteilNr
Lieferung(*LieferantenNr*, *BteilNr*, Anzahl, Bearbeiter)
Bauteil(*BteilNr*, BauteilBez)
Lieferant(*LieferantNr*, Ort)
Ort(*Ort*, Entf)

**BCNF**: true;
Lieferung(*LieferantenNr*, Anzahl, *Bearbeiter*)
Bauteil(*BteilNr*, BauteilBez)
Lieferant(*LieferantNr*, Ort)
Ort(*Ort*, Entf)
Bearbeiter(*Bearbeiter*, BteilNr)

## Aufgabe Normalisierung Lieferant
Bootsname -> Segelfläche
(Regattaname,Bootsname)->Besatzung
Regattaname ->Start,Länge,Ziel
(Start,Ziel)->Länge

**1NF**?: true; 
2NF: false:
   - RName -> Start, Ziel, Länge
   - Bootsname -> Segelfläche
Regatta(*Bootsname*, Segelfläche, Besatzung, *RName*, Start, Ziel, Länge)

**2NF**: true
3NF: false:
- (Start,Ziel)->Länge

Boot(*Bootsname*, Segelfläche)
Regatta(*RName*,Start,Ziel,Länge)
Teilname(*Bootsname*, *RName*,Besatzung)

**3NF**:true
**BCNF**: true
Strecke(*Start*, *Ziel*, Länge)
Boot(*Bootsname*, Segelfläche)
Teilname(*Bootsname*, *RName*,Besatzung)
Regatta(*RName*,Start,Ziel)


## Aufgabe Normalisierung Prüfungen
MatrNr -> Name, Vorname, Geb, FBNr, FBName
FBNr -> FBName
PNr->Fach,Prüfer,Termin
MatrNr, PNr -> Note

1NF?: false
Prüfungsanm(MatNr,Name,Vorname,Geb,FBNr,FBName,{Prüfung(PNr,Fach,Prüfer,Termin,Note)})

**1NF**: ja
2NF: nein: 
  - PNr->Fach,Prüfer,Termin 
Student(*MatrNr*, Name, Vorname, Geb, FBNr, FBName)
Prüfungteilnahme(*MatrNr*, *PNr*, Fach,Prüfer,Termin, Note)

**2NF**: ja
3NF: FBNr -> FBName
Student(*MatrNr*, Name, Vorname, Geb, FBNr, FBName)
Prüfung(*PNr*, Fach, Prüfer, Termin)
Teilnahme(*MatrNr*, *PNr*, Note)

**3NF**:true
**BCNF**: ja!
Student(*MatrNr*, Name, Vorname, Geb, FBNr)
Fachbereich(*FBNr*, FBName)
Teilnahme(*MatrNr*, *PNr*, Note)
Prüfung(*PNr*, Fach, Prüfer, Termin)






