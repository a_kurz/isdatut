# Tut6: Relationale Algebra
- [Quizizz](https://quizizz.com/admin/quiz/60afb666e98a7e001b7b8602)
- ! Anmeldung **in diesem Semester** im ersten Prüfugnszeitraum per konkludentem Verhalten, also **"Anmeldung durch Teilnahme"**. Die Studis müssen nichts anderes machen, als am Test teilzunehmen !
- **1. Test**: Probetest; Offizielle Infos folgen auf ISIS


## Wichtige Operationen
### Vereinigung
Sammelt Elemente (Tupel) zweier Relationen unter auf.


### Differenz
Eliminiert die Tupel (Zeilen) aus der ersten Relation, die auch in der zweiten Relation vorkommen.


### Schnittmenge
Tupel, die in beiden Relationen
gemeinsam vorkommen.


### Projektion
Erzeugt neue Relation mit einer Teilmenge der ursprünglichen Attribute (Spalten).


### Selektion
Erzeugt neue Relation mit gleichem Schema aber einer Teilmenge der Tupel (Zeilen).


### Kartesisches Produkt
R x S: Menge aller Tupel, die man erhält, wenn man jedes Tupel aus R mit jedem Tupel aus S paart.

### Umbenennung

### Natürlicher Join
Beim natürlichen Verbund werden die Datensätze der beiden beteiligten Tabellen miteinander verknüpft, deren Werte an den gleichnamigen Attributen übereinstimmen. 

### Aggregation
Fasst Werte einer Spalte zusammen.

### Gruppierung
Partitionierung der Tupel einer Relation gemäß ihrer Werte in einem oder mehr Attributen.
&rarr; Hauptzweck: Aggregation auf Teilen einer Relation (Gruppen).

### Sortierung

## Reframe Übersicht

|Konzept|Relationale Algebra|Reframe|
|-|-|-|
|Vereinigung|⋃|rel1.union(rel2)|
|Differenz|- backslash|rel1.minus(rel2)|
|Schnittmenge|∩| rel1.intersect.(rel2)|
|Projektion|π|rel1.project(['attr1','attr2])|
|Selektion|σ|rel1.select('attr1=="value1"')|
|Kartesisches Produkt|×|rel1.cartesian_product(rel2)|
|Umbenennung|ρ|rel.rename('old','New')|
|Natürlicher Join|⨝ | rel1.njoin(rel2)|
|Aggregation||  rel.count('column'),rel.min('column')|
|Gruppierung| γ|rel.groupby('attr')|

