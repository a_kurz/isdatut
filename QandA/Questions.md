# Q & A

### Logische Reihenfolge SQL Teil-Statements
*Question*: ORDER BY oder SELECT, was kommt zuerst?

*Answer*: 

- Wichtige Anmerkung: SQL ist eine deklarative Sprache. Das bedeut:
   - "SQL has no order of execution. Is a declarative language. The optimizer is free to choose any order it feels appropriate to produce the best execution time. Given any SQL query, is basically impossible to anybody to pretend it knows the execution order" - [Remus Rusanu](https://stackoverflow.com/a/4596739)
- Dadurch lassen sich keine tatsächlichen Ausführungspläne festlegen, nur logische Ausführungspläne die **meistens** so gelten.
- Microsoft gibt eine [Reihenfolge](https://docs.microsoft.com/en-us/sql/t-sql/queries/select-transact-sql?redirectedfrom=MSDN&view=sql-server-ver15#logical-processing-order-of-the-select-statement) an, in der SELECT vor ORDER BY kommt
- Andere Angeben sprechen davon, dass logisch gesehen ORDER BY vor SELECT kommen muss, da auch nach Attributen sortiert werden kann, die man in SELECT durch die Projektion entfernt

&rarr; Es lässt sich zusammenfassend sagen, dass meine keine fixe Reihenfolge festlegen kann und versuchen sollte sich mehr auf die Semantik der Anfragen zu konzentrieren als auf die Reihenfolge der Ausführung. Darum geht es nämlich bei deklarativen Sprachen.

### Hierarchical Clustering: Cluster mit gleichem Abstand und Dendogramme
Die Frage/Antwort bezieht sich auf agglomeratives hierarchisches Clustering.
Im Tutorium kam die Frage auf, wie das Dendogramm denn aussehen würde, wenn alle Cluster den gleichen Abstand hätte? Wäre es dann flach?
Die Antwort ist: Ja. Wenn mehrere Cluster den selben Abstand haben und dieser Abstand der geringste Abstand ist, dann werden diese Cluster alle gleichzeitig verschmelzt/gemergt. Ein Beispiel dafür könnt ihr in Folie 9 und 10 im VL-Foliensatz 'ISDA\_09\_Datenanalyse\_Explorativ\_vs\_Praediktiv.pdf' sehen - sowohl in der Tabelle als auch im Dendogramm:

![Screenshot VL-Folien](2.png)
