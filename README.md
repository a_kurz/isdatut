# ISDA Tut Aaron

Hi! Das hier ist das Git Repository zum wöchentlichen ISDA Tutorium von mir, Aaron. Hier findet ihr Lösungen, Links, Tipps...

- falls ihr **Fragen** habt, könnt ihr euch natürlich gerne jederzeit [per **E-Mail** bei mir melden](mailto:aaron.f.kurz@campus.tu-berlin.de) (schaut aber bitte zuerst in den Folien und im Forum nach, ob eure Frage schon beantwortet wurde)
    - alternativ könnt ihr mich auch per **TU-Chat/Matrix** erreichen: [@aaronkurz:matrix.tu-berlin.de](https://matrix.to/#/@aaronkurz:matrix.tu-berlin.de)
- ihr könnt gerne auch [**anonymes Feedback**/Wünsche/Ideen](https://forms.gle/kb3q4z9HzvUmPYbHA) geben

Achtung: Dieses Repository wurde von mir persönlich erstellt und steht in keiner offiziellen Verbindung zum ISDA-wModul und zum DIMA-Fachbereich. Für offizielle Infos schaut bitte immer in den ISIS-Kurs zum Modul.

Alte [Tutoriumsfolien Sommersemester 2019](https://git.tu-berlin.de/aaronkurz/isdatutoriumsfolienss2019).